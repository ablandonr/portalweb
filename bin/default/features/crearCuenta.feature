#Author: PITTLeasing@bancolombia.com.co
#language:es
@CreacionCuentas
CaracterÝstica: Creacion de cuentas de clientes
  Como cliente quiero crear cuentas con diferentes roles

  @CuentaClienteNatural
  Esquema de escenario: Como cliente natural deseo crear una cuenta
    Dado I want to write a step with <name>
    Cuando I check for the <value> in step
    Entonces I verify the <status> in step

    Ejemplos: 
      | name  | value | status  |
      | name1 |     5 | success |