package com.bancolombia.certificacion.questions;

import com.bancolombia.certificacion.userinterface.LeasingBancolombia;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Mensaje implements Question<String>{

	@Override
	public String answeredBy(Actor PITT) {
		return Text.of(LeasingBancolombia.MSJ_CREADA).viewedBy(PITT).asString();
	}

	public static Mensaje aVerificar() {
		return new Mensaje();
	}
	

}
