package com.bancolombia.certificacion.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://sbmdegwb10v:85/adm/cuentas/frmconsultarcuentas.aspx?")
public class LeasingBancolombia extends PageObject{
	public static final Target LBL_CREAR_CUENTA = Target.the("Crear una cuenta").
			locatedBy("//a[@href='/adm/cuentas/frmcrearcuenta.aspx?op=menu']");
	public static final Target CMP_IDENTIFICACION = Target.the("Ingresar la identificacion del usuario").
			located(By.id("ContentPlaceHolderContenido_ContentPlaceHolderContenido_ContentPlaceHolderContenido_TxtIdentificacion"));
	public static final Target BTN_CREAR = Target.the("Click en el boton de crear la cuenta").
			located(By.id("ContentPlaceHolderContenido_ContentPlaceHolderContenido_ContentPlaceHolderContenido_BtnCrear"));
	public static final Target CMP_CORREO = Target.the("Ingresar el correo del usuario").
			located(By.id("ContentPlaceHolderContenido_ContentPlaceHolderContenido_ContentPlaceHolderContenido_TxtEmail"));
	public static final Target CBX_CLIENTE = Target.the("Seleccionar si es cliente").
			located(By.id("ContentPlaceHolderContenido_ContentPlaceHolderContenido_ContentPlaceHolderContenido_ChkRoles_2"));
	public static final Target BTN_CREAR_FINAL = Target.the("Crear la cuenta").
			located(By.id("ContentPlaceHolderContenido_ContentPlaceHolderContenido_ContentPlaceHolderContenido_BtnCrear"));
	public static final Target BTN_SI = Target.the("Dar click si").
			located(By.id("ContentPlaceHolderContenido_VentanaConfirmacion1_wdwConfirmar_tmpl_BtnSi"));
	
	
	public static final Target MSJ_CREADA = Target.the("Mensaje de que la cuenta se ha creado").
			located(By.id("ContentPlaceHolderContenido_VentanaConfirmacion1_wdwConfirmar_tmpl_lblMensajeConfirmacion"));
	
}
