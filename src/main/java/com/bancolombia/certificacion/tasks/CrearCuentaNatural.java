package com.bancolombia.certificacion.tasks;

import java.util.List;

import com.bancolombia.certificacion.sucursalvirtual.model.Informacion;
import com.bancolombia.certificacion.userinterface.LeasingBancolombia;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class CrearCuentaNatural implements Task{

	List<Informacion> conInformacion;
	public CrearCuentaNatural(List<Informacion> conInformacion) {
		this.conInformacion = conInformacion;
	}

	@Override
	public <T extends Actor> void performAs(T PITT) {
		PITT.attemptsTo(Click.on(LeasingBancolombia.LBL_CREAR_CUENTA));
		PITT.attemptsTo(Enter.theValue(conInformacion.get(0).getNit()).into(LeasingBancolombia.CMP_IDENTIFICACION));
		PITT.attemptsTo(Click.on(LeasingBancolombia.BTN_CREAR));
		PITT.attemptsTo(Enter.theValue(conInformacion.get(0).getCorreo()).into(LeasingBancolombia.CMP_CORREO));
		PITT.attemptsTo(Click.on(LeasingBancolombia.CBX_CLIENTE));
		PITT.attemptsTo(Click.on(LeasingBancolombia.BTN_CREAR_FINAL));
		PITT.attemptsTo(Click.on(LeasingBancolombia.BTN_SI));
		PITT.attemptsTo(Click.on(LeasingBancolombia.LBL_CREAR_CUENTA));
		PITT.attemptsTo(Enter.theValue(conInformacion.get(0).getNit()).into(LeasingBancolombia.CMP_IDENTIFICACION));
		PITT.attemptsTo(Click.on(LeasingBancolombia.BTN_CREAR));
	}

	public static CrearCuentaNatural clickCrearCuenta(List<Informacion> conInformacion) {
		return Tasks.instrumented(CrearCuentaNatural.class, conInformacion);
	}
	

}
