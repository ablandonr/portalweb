package com.bancolombia.certificacion.tasks;

import com.bancolombia.certificacion.userinterface.LeasingBancolombia;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task{

	private LeasingBancolombia leasingBancolombia;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(leasingBancolombia));
	}

	public static Abrir laPaginaDelBanco() {
		return Tasks.instrumented(Abrir.class);
	}

}

