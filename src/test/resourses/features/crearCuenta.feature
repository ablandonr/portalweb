#Autor
#language:es
@CreacionCuentas
Característica: Creacion de cuentas de clientes
  Como cliente quiero crear cuentas con diferentes roles

  @CuentaClienteNatural
  Escenario: Como cliente natural deseo crear una cuenta
    Dado que yo deseo crear una cuenta de cliente natural 
    Cuando se ingresa los datos requeridos
    |nit|correo|
    |8903041304|ablando@bancolombia.com.co|
    Entonces aparece el mensaje de confirmacion La cuenta ya existe. ¿Desea ir administrar su información?

 