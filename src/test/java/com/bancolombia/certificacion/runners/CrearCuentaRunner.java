package com.bancolombia.certificacion.runners;

import org.junit.runner.RunWith;
import com.bancolombia.certificacion.runners.CrearCuentaRunner;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (
		features = "src/test/resourses/features/crearCuenta.feature",
		tags= "@CuentaClienteNatural",
		glue="com.bancolombia.certificacion.stepdefinitions", 
		snippets= SnippetType.CAMELCASE)

public class CrearCuentaRunner {
	

}
