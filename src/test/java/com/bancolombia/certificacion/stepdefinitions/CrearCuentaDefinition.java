package com.bancolombia.certificacion.stepdefinitions;

import java.util.List;

import org.openqa.selenium.WebDriver;

import com.bancolombia.certificacion.questions.Mensaje;
import com.bancolombia.certificacion.sucursalvirtual.model.Informacion;
import com.bancolombia.certificacion.tasks.CrearCuentaNatural;
import com.bancolombia.certificacion.userinterface.LeasingBancolombia;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class CrearCuentaDefinition {

	@Managed(driver="chrome")
	WebDriver navegadorChrome;
	
	@Before
	public void configuracionInicial() {
		OnStage.setTheStage(new OnlineCast());
		theActorCalled("usuario").can(BrowseTheWeb.with(navegadorChrome));
	}
	
	@Dado("^que yo deseo crear una cuenta de cliente natural$")
	public void queEstasEnLaPaginaDelBanco() throws Exception {
		theActorInTheSpotlight().wasAbleTo(Open.browserOn(new LeasingBancolombia()));
	}

	@Cuando("^se ingresa los datos requeridos$")
	public void seIngresaLosDatosRequeridos(List<Informacion> conInformacion) throws Exception {
		theActorInTheSpotlight().attemptsTo(CrearCuentaNatural.clickCrearCuenta(conInformacion));
	}

	@Entonces("^aparece el mensaje de confirmacion (.*)$")
	public void seCreaLaCuenta(String msjEsperado) throws Exception {
		theActorInTheSpotlight().should(seeThat(Mensaje.aVerificar(),equalTo(msjEsperado)));
	}
}
